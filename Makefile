PREFIX ?= /usr/local
DOCDIR ?= $(PREFIX)/share/doc/mkinitcpio-decrypt-luks
LIBDIR ?= $(PREFIX)/lib/initcpio

install:

	install -d '$(DESTDIR)$(LIBDIR)/hooks'
	install -t '$(DESTDIR)$(LIBDIR)/hooks'   -m 755 hooks/decrypt-luks

	install -d '$(DESTDIR)$(LIBDIR)/install'
	install -t '$(DESTDIR)$(LIBDIR)/install' -m 755 install/decrypt-luks

	install -d '$(DESTDIR)$(DOCDIR)'
	install -t '$(DESTDIR)$(DOCDIR)'         -m 644 README

uninstall:

	rm -f  '$(DESTDIR)$(LIBDIR)/hooks/decrypt-luks'
	rm -f  '$(DESTDIR)$(LIBDIR)/install/decrypt-luks'

	rm -rf '$(DESTDIR)$(DOCDIR)'

.PHONY: install uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=78:
